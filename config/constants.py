from enum import Enum, auto

class Constants():
    # HOST ="140.207.81.249"
    # HOST ="192.168.1.107"
    HOST ="localhost"

    PORT_PUBLIC = "8991"
    PORT_AUTH = "8993"

    USER_NAME = "testTrader"
    USER_PASSWORD = "66666"

    SUBACCOUNT_CODE_HUOBI = "huobi_a_001"
    SUBACCOUNT_CODE_OKEX = "okex_hz_001"
    SUBACCOUNT_CODE_BITFINEX = "bitfinex_a_001"
    SUBACCOUNT_CODE_BINANCE = "biance_a_001"


    _ONE_DAY_IN_SECONDS = 60 * 60 * 24

    class OrderType(Enum):
        LIMIT = 'limit'
        MARKET = 'market'

    class OrderSide(Enum):
        BUY = 'buy'
        SELL = 'sell'

    class Exchanges(Enum):
        HUOBI = 'huobi'
        BINANCE = 'binance'
        OKEX = 'okex'
        BITFINEX = 'bitfinex'

    class OrderStatus(Enum):
        INSERT_SUCCESS = auto()
        INSERT_CANCEL_SUCCESS = auto()