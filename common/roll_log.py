# -*- coding: utf8 -*-
import logging
import os
from logging.handlers import TimedRotatingFileHandler

when = 'D'
backupCount = 14
log_path = os.path.abspath('.')


def init_roll_log(log_file='roll.log', log_level=logging.DEBUG):
    """
    按日期切分日志，过期则删

    :param log_file: log文件名
    :param log_level: log日志级别
    :return:
    """

    good_log_path = '{}{}/'.format(log_path, '/output')
    time_rthandler = TimedRotatingFileHandler(filename=good_log_path + log_file, when=when, backupCount=backupCount)
    formatter = logging.Formatter('%(asctime)s %(filename)s:%(lineno)d <%(thread)d> %(levelname)-8s %(message)s')
    time_rthandler.setFormatter(formatter)
    log_obj = logging.getLogger(__name__)
    log_obj.addHandler(time_rthandler)
    log_obj.setLevel(log_level)
    return log_obj
