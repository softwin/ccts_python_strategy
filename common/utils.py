from config.constants import Constants

class SymbolCoordinator:
    def __init__(self, baseCurrency, quoteCurrency):
        self.baseCurrency = baseCurrency
        self.quoteCurrency = quoteCurrency

    def get_quote_currency(self, exchange):
        if self.quoteCurrency == 'usdt' and exchange == Constants.Exchanges.BITFINEX:
            return 'usd'
        else:
            return self.quoteCurrency

    def get_base_currency(self, exchange):
        if self.baseCurrency == 'bch' and exchange == Constants.Exchanges.BINANCE:
            return 'bcc'
        else:
            return self.baseCurrency