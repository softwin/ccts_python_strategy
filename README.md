### 运行方法
1. 安装依赖：pip install -r requirements.txt
2. 生成接口代码：generate_protos.bat
3. 在config/constants.py中配置broker系统的地址、端口、你的用户名、密码和子账号码
4. 在main.py中配置想要运行的策略
5. 运行：python main.py

### 策略介绍 - ma_strategy.py
1. 订阅Huobi的Tick
2. 计算快速平均线（周期为5个tick）和慢速平均线（周期为30个tick）
3. 快速平均线上穿慢速平均线时，取消所有卖单，下一个买单。反之同理。

### 策略介绍 - position_strategy.py
1. 订阅Huobi, OKEX, BINANCE, BITFINEX的Depth
2. 比较盘口差价，如果有利润时（差价大于手续费），在最低卖价处下买单，在最高买价处下卖单